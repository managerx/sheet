<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use Illuminate\Support\Facades\Storage;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::check()) {
          
        $messages = Message::all();

        return view('messages.index',['messages'=>$messages]);
        }
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (Auth::check()) {
            
            Storage::putFileAs('public',$request->file('file'),$request->file('file')->getClientOriginalName());
            $message = message::create([

                'titre'=> $request->input('titre'),
                'description'=> $request->input('description'),
                'user_id'=> Auth::user()->id,
                'file'=> $request->file('file')->getClientOriginalName(),
                ]);

        if ($message) {


            return redirect()->route('messages.index', ['message'=> $message->id])->with('success','tweet created successfully');
        }
        }
         return back()->withinput()->with('errors','you should be connected'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
        
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
        $message = Message::find($message->id);
        
        return view('messages.edit',['message'=> $message]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        Storage::putFileAs('public',$request->file('file'),$request->file('file')->getClientOriginalName());
         $messageUpdate = message::where('id',$message->id)->update([
            'titre'=>$request->input('titre'),
            'description'=>$request->input('description'),
            'file'=>$request->file('file')->getClientOriginalName(),

            ]);
        if($messageUpdate){
            return redirect()->route('messages.index',['message'=>$message->id])->with('success','message update successfully');
        }

        //redirect
        return back()->withinput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
         $findmessage = Message::find($message->id);
        if ($findmessage->delete()) {
            return redirect()->route('messages.index')->with('success','message deleted successfully');
        }
        return back()->withinput()->with('error','message could not be deleted');
    }

    public function postLikePost(Request $request)
    {
        $post_id = $request['postId'];
        $is_like = $request['isLike'] === 'true';
        $update = false;
        $post = Post::find($post_id);
        if (!$post) {
            return null;
        }
        $user = Auth::user();
        $like = $user->likes()->where('post_id', $post_id)->first();
        if ($like) {
            $already_like = $like->like;
            $update = true;
            if ($already_like == $is_like) {
                $like->delete();
                return null;
            }
        } else {
            $like = new Like();
        }
        $like->like = $is_like;
        $like->user_id = $user->id;
        $like->post_id = $post->id;
        if ($update) {
            $like->update();
        } else {
            $like->save();
        }
        return null;
    }
}
