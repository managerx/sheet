@extends('layouts.app')

@section('content')
<div class="col-md-6 col-lg-6 col-md-offset-3  col-lg-offset-3">
    <div class="panel panel-primary ">
  <div class="panel-heading">Messsages<a class="pull-right" href="/messages/create"  style="color: black ">Ajouter</a></div> 
  <div class="panel-body">
    <ul class="list-group">

 		@foreach($messages as $message)
          <!-- Title -->
          	
         			 <div class="comment-box">
						<div class="comment-head">
							<h6 class="comment-name by-author"><a href="#">{{$message->user->name}}</a></h6>
						</div>
             <label>Titre : {{$message->titre}}</label> 
						<div class="comment-content">
							{{$message->description}}
						</div>
            <div class="comment-content">
               <a href="{{url('/')}}{{ Storage::disk('local')->url($message->file)}}">File : {{$message->file}}</a>
            </div>
						
					 <!--<label style="color: blue">{{$message->total}}  <i class="far fa-thumbs-up"></i></label>&nbsp;&nbsp;
           
           @if( $message->user->name == 'ayoub' )
           <a href="#">like</a>
           @endif-->

					 <span >  on {{$message->created_at}}</span> 

           @if( Auth::user()->id == $message->user_id)
					 <a   
              href="{{ route('messages.destroy',[$message->id]) }}"
                  onclick="
                  var result = confirm('Are you sure you wish to delete this message?');
                      if( result ){
                              event.preventDefault();
                              document.getElementById('delete-form').submit();
                      }
                          "
                          >
                  Delete
              </a>
              <a href="/messages/{{$message->id}}/edit">Edit</a>
              @endif
              <form id="delete-form" action="{{ route('messages.destroy',[$message->id]) }}" 
                method="POST" style="display: none;">
                        <input type="hidden" name="_method" value="delete">
                        {{ csrf_field() }}
              </form>
					<hr>
					</div>

		@endforeach


</ul>
  </div>
</div>
</div>

@endsection

